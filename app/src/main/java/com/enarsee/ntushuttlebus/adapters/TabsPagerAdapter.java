package com.enarsee.ntushuttlebus.adapters;



        import com.enarsee.ntushuttlebus.redFragment;
        import com.enarsee.ntushuttlebus.blueFragment;
        import com.enarsee.ntushuttlebus.riderFragment;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new blueFragment();
            case 1:
                // Games fragment activity
                return new redFragment();
            case 2:
                // Movies fragment activity
                return new riderFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }

}